from . import Common
from . import Backend
from . import Writers

import os
import logging
logger = logging.getLogger('ProjectBuilder')


class ProjectBuilder:
    def __init__(self, args=None):
        self.context  = Common.Context(args)
        self.__setup_logger()

        self.solution = Backend.Structure.Solution()
        self.solution.add_global_variable('SourceDirectory', os.path.dirname(self.context.args.input))
        self.solution.add_global_variable('DestinationDirectory', self.context.args.output)

    def __setup_logger(self):
        logger.setLevel(self.context.args.log_level)

        formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(name)s: %(message)s')

        file_handler = logging.FileHandler(self.context.args.log_path, 'w')
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)

        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(formatter)
        logger.addHandler(stream_handler)

    def run(self):
        try:
            Backend.Loader.load_solution(self.solution, self.context.args.input)
        except Backend.ParseError as parse_error:
            logger.error('Parsing error: {}'.format(parse_error))

        if self.context.args.start_up is not None:
            for project in self.solution.projects:
                if self.context.args.start_up == project.name:
                    self.solution.projects.insert(0, self.solution.projects.pop(self.solution.projects.index(project)))
                    break

        Writers.VSProjectWriter.write_solution(self.solution, self.context)


if __name__ == '__main__':
    builder = ProjectBuilder()
    builder.run()
