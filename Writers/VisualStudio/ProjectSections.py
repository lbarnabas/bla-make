import xml.etree.ElementTree as Xml


class ProjectConfigurationSection:
    def __init__(self, platform_name, config_name):
        self.platform_name = platform_name
        self.config_name = config_name

    def serialize(self, parent_element):
        project_configuration = Xml.SubElement(parent_element, 'ProjectConfiguration', {
            'Include': '{}|{}'.format(self.config_name, self.platform_name)
        })
        Xml.SubElement(project_configuration, 'Configuration').text = self.config_name
        Xml.SubElement(project_configuration, 'Platform').text = self.platform_name


class ProjectConfigurationsSection:
    def __init__(self, platform_name):
        self.platform_name = platform_name
        self.configs = []

    def add_config(self, config_name):
        self.configs.append(ProjectConfigurationSection(self.platform_name, config_name))

    def serialize(self, parent_element):
        project_configurations = Xml.SubElement(parent_element, 'ItemGroup', {'Label': 'ProjectConfigurations'})
        for configuration in self.configs:
            configuration.serialize(project_configurations)


class ReferencesSection:
    def __init__(self):
        self.project_references = []

    def add_project(self, project_guid, project_path):
        self.project_references.append([project_guid, project_path])

    def serialize(self, parent_element):
        if len(self.project_references) > 0:
            references_group = Xml.SubElement(parent_element, 'ItemGroup')
            for guid, path in self.project_references:
                project_reference = Xml.SubElement(references_group, 'ProjectReference', {
                    'Include': path
                })
                Xml.SubElement(project_reference, 'Project').text = '{{{}}}'.format(guid)


class GlobalsSection:
    def __init__(self):
        self.vc_project_version = '15.0'
        self.project_guid = None
        self.keyword = 'Win32Proj'
        self.root_namespace = None
        self.windows_target_platform_version = '10.0.17134.0'

    def set_project_guid(self, guid):
        self.project_guid = '{{{}}}'.format(guid)

    def set_root_namespace(self, namespace):
        self.root_namespace = namespace

    def serialize(self, parent_element):
        self_group = Xml.SubElement(parent_element, 'PropertyGroup', {'Label': 'Globals'})
        Xml.SubElement(self_group, 'VCProjectVersion').text = self.vc_project_version
        Xml.SubElement(self_group, 'ProjectGuid').text = self.project_guid
        Xml.SubElement(self_group, 'Keyword').text = self.keyword
        Xml.SubElement(self_group, 'RootNamespace').text = self.root_namespace
        Xml.SubElement(self_group, 'WindowsTargetPlatformVersion').text = self.windows_target_platform_version


class CppDefaultPropSection:
    def __init__(self, platform_name, config_name):
        self.platform_name = platform_name
        self.config_name = config_name

        self.configuration_type = None
        self.use_debug_libraries = None
        self.set_use_debug_libraries(config_name == 'Debug')
        self.platform_toolset = 'v141'
        self.whole_program_optimization = None
        if config_name == 'Release':
            self.set_whole_program_optimization(True)
        self.character_set = 'Unicode'

    def set_configuration_type(self, config_type):
        self.configuration_type = config_type

    def set_use_debug_libraries(self, enabled):
        self.use_debug_libraries = 'true' if enabled else 'false'

    def set_whole_program_optimization(self, enabled):
        self.whole_program_optimization = 'true' if enabled else 'false'

    def serialize(self, parent_element):
        property_group = Xml.SubElement(parent_element, 'PropertyGroup', {
                'Condition': "'$(Configuration)|$(Platform)'=='{}|{}'".format(self.config_name, self.platform_name),
                'Label': 'Configuration'
            })
        Xml.SubElement(property_group, 'ConfigurationType').text = self.configuration_type
        Xml.SubElement(property_group, 'UseDebugLibraries').text = self.use_debug_libraries
        Xml.SubElement(property_group, 'PlatformToolset').text = self.platform_toolset
        if self.whole_program_optimization is not None:
            Xml.SubElement(property_group, 'WholeProgramOptimization').text = self.whole_program_optimization
        Xml.SubElement(property_group, 'CharacterSet').text = self.character_set


class CppDefaultPropsSection:
    def __init__(self, platform_name):
        self.platform_name = platform_name
        self.props = []

    def create_default_prop(self, config_name):
        return CppDefaultPropSection(self.platform_name, config_name)

    def add_default_prop(self, prop):
        self.props.append(prop)

    def serialize(self, parent_element):
        Xml.SubElement(parent_element, 'Import', {
            'Project': '$(VCTargetsPath)\\Microsoft.Cpp.Default.props'
        })
        for prop in self.props:
            prop.serialize(parent_element)


class CppPropsSection:
    def __init__(self):
        pass

    def serilize(self, parent_element):
        Xml.SubElement(parent_element, 'Import', {
            'Project': '$(VCTargetsPath)\\Microsoft.Cpp.props'
        })


class ExtensionSettingsSection:
    def __init__(self):
        pass

    def serialize(self, parent_element):
        Xml.SubElement(parent_element, 'ImportGroup', {
            'Label': 'ExtensionSettings'
        })


class SharedSection:
    def __init__(self):
        pass

    def serialize(self, parent_element):
        Xml.SubElement(parent_element, 'ImportGroup', {
            'Label': 'Shared'
        })


class PropertySheetSection:
    def __init__(self, platform_name, config_name):
        self.platform_name = platform_name
        self.config_name = config_name

    def serialze(self, parent_element):
        import_group = Xml.SubElement(parent_element, 'ImportGroup', {
            'Label': 'PropertySheets',
            'Condition': "'$(Configuration)|$(Platform)'=='{}|{}'".format(self.config_name, self.platform_name)
        })
        Xml.SubElement(import_group, 'Import', {
            'Project': '$(UserRootDir)\\Microsoft.Cpp.$(Platform).user.props',
            'Condition': "exists('$(UserRootDir)\\Microsoft.Cpp.$(Platform).user.props')",
            'Label': 'LocalAppDataPlatform'
        })


class UserMacrosSection:
    def __init__(self):
        pass

    def serialize(self, parent_element):
        Xml.SubElement(parent_element, 'PropertyGroup', {
            'Label': 'UserMacros'
        })


class LinkIncrementalSection:
    def __init__(self, platform_name, config_name):
        self.platform_name = platform_name
        self.config_name = config_name

        self.link_incremental = None
        self.set_link_incremental(config_name == 'Debug')

    def set_link_incremental(self, enabled):
        self.link_incremental = 'true' if enabled else 'false'

    def serialize(self, parent_element):
        property_group = Xml.SubElement(parent_element, 'PropertyGroup', {
            'Condition': "'$(Configuration)|$(Platform)'=='{}|{}'".format(self.config_name, self.platform_name)
        })
        Xml.SubElement(property_group, 'LinkIncremental').text = self.link_incremental


class CompilerSettingsSection:
    def __init__(self, config_name):
        self.precompiled_header = 'NotUsing'
        self.warning_level = 'Level3'
        self.optimization = None
        self.function_level_linking = None
        self.intrinsic_functions = None
        self.sdl_check = 'true'
        self.preprocessor_definitions = []
        self.conformance_mode = 'true'
        self.additional_directories = []
        self.precompiled_header_file = None

        self.set_optimization('MaxSpeed' if config_name == 'Release' else 'Disabled')
        if config_name == 'Release':
            self.set_function_level_linking(True)
            self.set_intrinsic_functions(True)

    def set_precompiled_header(self, file):
        self.precompiled_header = 'Use' if file is not None else 'NotUsing'
        self.precompiled_header_file = file

    def set_optimization(self, optimization):
        self.optimization = optimization

    def set_function_level_linking(self, enabled):
        self.function_level_linking = 'true' if enabled else 'false'

    def set_intrinsic_functions(self, enabled):
        self.intrinsic_functions = 'true' if enabled else 'false'

    def add_preprocessor_definition(self, definition):
        self.preprocessor_definitions.append(definition)

    def add_additional_directory(self, directory):
        self.additional_directories.append(directory)

    def serialize(self, parent_element):
        preprocessor_definitions = self.preprocessor_definitions + ['%(PreprocessorDefinitions)']
        additional_directories = self.additional_directories + ['$(SolutionDir)', '$(ProjectDir)']
        cl_compile = Xml.SubElement(parent_element, 'ClCompile')
        Xml.SubElement(cl_compile, 'PrecompiledHeader').text = self.precompiled_header
        Xml.SubElement(cl_compile, 'WarningLevel').text = self.warning_level
        Xml.SubElement(cl_compile, 'Optimization').text = self.optimization
        if self.function_level_linking is not None:
            Xml.SubElement(cl_compile, 'FunctionLevelLinking').text = self.function_level_linking
        if self.intrinsic_functions is not None:
            Xml.SubElement(cl_compile, 'IntrinsicFunctions').text = self.intrinsic_functions
        Xml.SubElement(cl_compile, 'SDLCheck').text = self.sdl_check
        Xml.SubElement(cl_compile, 'PreprocessorDefinitions').text = ';'.join(preprocessor_definitions)
        Xml.SubElement(cl_compile, 'ConformanceMode').text = self.conformance_mode
        Xml.SubElement(cl_compile, 'AdditionalIncludeDirectories').text = ';'.join(additional_directories)
        if self.precompiled_header_file is not None:
            Xml.SubElement(cl_compile, 'PrecompiledHeaderFile').text = self.precompiled_header_file


class LinkerSettingsSection:
    def __init__(self, config_name):
        self.sub_system = 'Console'
        self.enable_comdat_folding = None
        self.optimize_references = None
        self.generate_debug_information = 'true'

        if config_name == 'Release':
            self.set_enable_comdat_folding(True)
            self.set_optimize_references(True)

    def set_enable_comdat_folding(self, enabled):
        self.enable_comdat_folding = 'true' if enabled else 'false'

    def set_optimize_references(self, enabled):
        self.optimize_references = 'true' if enabled else 'false'

    def serialize(self, parent_element):
        link = Xml.SubElement(parent_element, 'Link')
        Xml.SubElement(link, 'SubSystem').text = self.sub_system
        if self.enable_comdat_folding is not None:
            Xml.SubElement(link, 'EnableCOMDATFolding').text = self.enable_comdat_folding
        if self.optimize_references is not None:
            Xml.SubElement(link, 'OptimizeReferences').text = self.optimize_references
        Xml.SubElement(link, 'GenerateDebugInformation').text = self.generate_debug_information


class CompilationSettingsSection:
    def __init__(self, platform_name, config_name):
        self.platform_name = platform_name
        self.config_name = config_name

        self.compiler_settings = CompilerSettingsSection(config_name)
        self.linker_settings = LinkerSettingsSection(config_name)

    def serialize(self, parent_element):
        definition_group = Xml.SubElement(parent_element, 'ItemDefinitionGroup', {
            'Condition': "'$(Configuration)|$(Platform)'=='{}|{}'".format(self.config_name, self.platform_name)
        })
        self.compiler_settings.serialize(definition_group)
        self.linker_settings.serialize(definition_group)


class CppTargetsSection:
    def __init__(self):
        pass

    def serialize(self, parent_element):
        Xml.SubElement(parent_element, 'Import', {
            'Project': '$(VCTargetsPath)\\Microsoft.Cpp.targets'
        })


class ExtensionTargets:
    def __init__(self):
        pass

    def serialize(self, parent_element):
        Xml.SubElement(parent_element, 'ImportGroup', {
            'Label': 'ExtensionTargets'
        })
