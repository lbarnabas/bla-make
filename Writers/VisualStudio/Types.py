from ... import Common

from ...Backend import Structure

import uuid
from enum import Enum


class ProjectType(Enum):
    Cpp = 1


project_type_guid_mapping = {
    ProjectType.Cpp: uuid.UUID('8BC9CEB8-8B4A-11D0-8D11-00A0C91BC942')
}


class Optimization(Enum):
    Disabled = 1,
    MaxSpeed = 2


optimization_mapping = {
    Optimization.Disabled: 'Disabled',
    Optimization.MaxSpeed: 'MaxSpeed'
}

config_mapping = {
    Common.Context.Config.Debug:   'Debug',
    Common.Context.Config.Release: 'Release'
}

solution_platform_mapping = {
    Common.Context.Platform.Win32: 'x86',
    Common.Context.Platform.Win64: 'x64'
}

project_platform_mapping = {
    Common.Context.Platform.Win32: 'Win32',
    Common.Context.Platform.Win64: 'x64'
}

platform_name_mapping = {
    Common.Context.Platform.Win32: 'Win32',
    Common.Context.Platform.Win64: 'x64'
}

project_type_mapping = {
    Structure.Project.OutputType.StaticLibrary: ProjectType.Cpp,
    Structure.Project.OutputType.DynamicLibrary: ProjectType.Cpp,
    Structure.Project.OutputType.Application: ProjectType.Cpp
}

project_output_type_mapping = {
    Structure.Project.OutputType.StaticLibrary: 'StaticLibrary',
    Structure.Project.OutputType.DynamicLibrary: 'DynamicLibrary',
    Structure.Project.OutputType.Application: 'Application'
}
