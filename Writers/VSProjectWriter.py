from .. import Common

from ..Backend import Structure

from .VisualStudio import Types
from .VisualStudio import ProjectSections

import os
import sqlite3
import pathlib
import contextlib
import xml.etree.ElementTree as Xml
from enum import Enum

import logging
logger = logging.getLogger('ProjectBuilder.VSWriter')

__all__ = ['write_solution']


class VSContext:
    def __init__(self, solution: Structure.Solution, context: Common.Context):
        self.solution = solution
        self.context = context

        os.makedirs(context.args.output, exist_ok=True)

        self.cache_db = sqlite3.connect(os.path.join(context.args.output, '.cache.db'))
        self.guids = Common.GuidCache(self.cache_db)


class VSProject:
    def __init__(self, project: Structure.Project, vs_solution, vs_context: VSContext):
        self.project = project
        self.vs_solution = vs_solution
        self.vs_context  = vs_context

    @property
    def name(self):
        return self.project.name

    @property
    def vs_type(self):
        assert(self.project.output_type in Types.project_type_mapping)
        return Types.project_type_mapping[self.project.output_type]

    @property
    def vs_type_guid(self):
        assert(self.vs_type in Types.project_type_guid_mapping)
        return Types.project_type_guid_mapping[self.vs_type]

    @property
    def guid(self):
        return self.vs_context.guids.get_guid('Projects-{0.name}'.format(self))

    @property
    def directory(self):
        return os.path.join(self.vs_context.context.args.output, self.name)

    @property
    def project_path(self):
        return os.path.abspath(os.path.join(self.directory, '{0.name}.vcxproj'.format(self)))

    @property
    def filters_path(self):
        return os.path.abspath(os.path.join(self.directory, '{0.name}.vcxproj.filters'.format(self)))

    @property
    def platform_name(self):
        assert(self.vs_context.context.platform in Types.project_platform_mapping)
        return Types.project_platform_mapping[self.vs_context.context.platform]

    @property
    def output_type(self):
        assert(self.project.output_type in Types.project_output_type_mapping)
        return Types.project_output_type_mapping[self.project.output_type]

    def write(self):
        os.makedirs(self.directory, exist_ok=True)
        self.__write_project()

    def __write_project(self):
        source_path = pathlib.Path(
            self.vs_context.solution.decode_globals('{SourceDirectory}', self.vs_context.context)
        ).absolute()
        project_path = pathlib.Path(
            self.vs_context.solution.decode_globals(self.project.directory, self.vs_context.context)
        ).absolute()

        project_root = Xml.Element('Project', {
            'DefaultTargets': 'Build',
            'ToolsVersion': '15.0',
            'xmlns': 'http://schemas.microsoft.com/developer/msbuild/2003'
        })
        filters_root = Xml.Element('Project', {
            'ToolsVersion': '4.0',
            'xmlns': 'http://schemas.microsoft.com/developer/msbuild/2003'
        })

        # Project Configurations
        project_configurations = ProjectSections.ProjectConfigurationsSection(self.platform_name)
        for config_name in Types.config_mapping.values():
            project_configurations.add_config(config_name)
        project_configurations.serialize(project_root)

        # Files
        precompiled_header = None
        files = self.project.get_files(self.vs_context.solution, self.vs_context.context)

        folder_stack = [(files, None)]

        header_group = Xml.SubElement(project_root, 'ItemGroup')
        source_group = Xml.SubElement(project_root, 'ItemGroup')

        filters_group = Xml.SubElement(filters_root, 'ItemGroup')
        header_filter_group = Xml.SubElement(filters_root, 'ItemGroup')
        source_filter_group = Xml.SubElement(filters_root, 'ItemGroup')

        while len(folder_stack) != 0:
            folder, filter_name = folder_stack.pop()

            if filter_name is not None:
                filter_element = Xml.SubElement(filters_group, 'Filter', {'Include': filter_name})
                Xml.SubElement(filter_element, 'UniqueIdentifier').text = '{{{}}}'.format(self.vs_context.guids.get_guid('Filter-{}/{}'.format(self.project.name, filter_name)))

            for file in folder.files:
                file_path = pathlib.Path(file.file_path).absolute()
                if file.file_type == Structure.File.Type.Header:
                    Xml.SubElement(header_group, 'ClInclude', {'Include': str(file_path)})
                    if file.pch == Structure.File.Pch.Header:
                        precompiled_header = str(pathlib.Path(file_path).relative_to(project_path))

                    if filter_name is not None:
                        filter_cl_include = Xml.SubElement(header_filter_group, 'ClInclude', {'Include': str(file_path)})
                        Xml.SubElement(filter_cl_include, 'Filter').text = filter_name

                elif file.file_type == Structure.File.Type.Source:
                    cl_compile = Xml.SubElement(source_group, 'ClCompile', {'Include': str(file_path)})
                    if file.pch == Structure.File.Pch.Create:
                        for config, config_name in Types.config_mapping.items():
                            Xml.SubElement(cl_compile, 'PrecompiledHeader', {'Condition': "'$(Configuration)|$(Platform)'=='{}|{}'".format(config_name, self.platform_name)}).text = 'Create'

                    if filter_name is not None:
                        filter_cl_compile = Xml.SubElement(source_filter_group, 'ClCompile', {'Include': str(file_path)})
                        Xml.SubElement(filter_cl_compile, 'Filter').text = filter_name

            for sub_folder in folder.folders:
                folder_stack.append((sub_folder, sub_folder.name if filter_name is None else '{}\\{}'.format(filter_name, sub_folder.name)))

        # References
        references = ProjectSections.ReferencesSection()
        for dependency in self.project.dependencies:
            referenced_project = self.vs_solution.get_project(dependency)
            references.add_project(referenced_project.guid, referenced_project.project_path)
        references.serialize(project_root)

        # Globals
        globals_section = ProjectSections.GlobalsSection()
        globals_section.set_project_guid(self.guid)
        globals_section.set_root_namespace(self.name)
        globals_section.serialize(project_root)

        # CXX default properties
        cpp_default_props = ProjectSections.CppDefaultPropsSection(self.platform_name)
        for config, config_name in Types.config_mapping.items():
            prop = cpp_default_props.create_default_prop(config_name)
            prop.set_configuration_type(self.output_type)
            cpp_default_props.add_default_prop(prop)
        cpp_default_props.serialize(project_root)

        # CXX properties
        cpp_props = ProjectSections.CppPropsSection()
        cpp_props.serilize(project_root)

        # Extension settings
        extension_settings = ProjectSections.ExtensionSettingsSection()
        extension_settings.serialize(project_root)

        # Shared
        shared_settings = ProjectSections.SharedSection()
        shared_settings.serialize(project_root)

        # Property sheets
        for config, config_name in Types.config_mapping.items():
            property_sheet = ProjectSections.PropertySheetSection(self.platform_name, config_name)
            property_sheet.serialze(project_root)

        # User macros
        user_macros = ProjectSections.UserMacrosSection()
        user_macros.serialize(project_root)

        # Link incremental settings
        for config, config_name in Types.config_mapping.items():
            linker_settings = ProjectSections.LinkIncrementalSection(self.platform_name, config_name)
            linker_settings.serialize(project_root)

        # Compilation settings
        for config, config_name in Types.config_mapping.items():
            self.vs_context.context.config = config
            compilation_settings = ProjectSections.CompilationSettingsSection(self.platform_name, config_name)

            for project_include in self.project.get_includes(self.vs_context.solution, self.vs_context.context):
                include_path = pathlib.Path(self.vs_context.solution.decode_globals(project_include.directory, self.vs_context.context)).absolute()
                compilation_settings.compiler_settings.add_additional_directory(str(include_path))
            compilation_settings.compiler_settings.add_additional_directory(str(source_path))
            compilation_settings.compiler_settings.add_additional_directory(str(project_path))

            if self.vs_context.context.platform == Common.Context.Platform.Win32:
                compilation_settings.compiler_settings.add_preprocessor_definition('WIN32')
            if config == Common.Context.Config.Debug:
                compilation_settings.compiler_settings.add_preprocessor_definition('_DEBUG')
            if config == Common.Context.Config.Release:
                compilation_settings.compiler_settings.add_preprocessor_definition('NDEBUG')
            if self.project.output_type == Structure.Project.OutputType.StaticLibrary:
                compilation_settings.compiler_settings.add_preprocessor_definition('_LIB')

            compilation_settings.compiler_settings.set_precompiled_header(precompiled_header)

            compilation_settings.serialize(project_root)

        # CXX targets
        cpp_targets = ProjectSections.CppTargetsSection()
        cpp_targets.serialize(project_root)

        # Extension targets
        extension_targets = ProjectSections.ExtensionTargets()
        extension_targets.serialize(project_root)

        # Write xmls
        Common.Utils.write_xml(project_root, self.project_path)
        Common.Utils.write_xml(filters_root, self.filters_path)


class VSSolution:
    def __init__(self, vs_context: VSContext):
        self.vs_context = vs_context

        filename = vs_context.context.args.name if vs_context.context.args.name \
            else '{0}_{1}'.format(self.vs_context.solution.name, self.vs_context.context.args.platform)
        self.file_path = os.path.join(vs_context.context.args.output, filename + '.sln')
        logger.info('Solution file: {}'.format(self.file_path))

        self.projects = []
        for project in self.vs_context.solution.projects:
            self.projects.append(VSProject(project, self, self.vs_context))

    @property
    def guid(self):
        return self.vs_context.guids.get_guid('Solution-')

    @property
    def platform_name(self):
        return Types.solution_platform_mapping[self.vs_context.context.platform]

    def get_project(self, project_name: str):
        for project in self.projects:
            if project_name == project.project.name:
                return project

    class ProjectContext(contextlib.ContextDecorator):
        def __init__(self, project: VSProject, output):
            self.project = project
            self.output = output

        def __enter__(self):
            self.output.write('Project("{{{0.vs_type_guid}}}") = "{0.name}", "{0.project_path}", "{{{0.guid}}}"\n'.format(self.project))
            return self

        def __exit__(self, *e):
            self.output.write('EndProject\n')

    class GlobalContext(contextlib.ContextDecorator):
        def __init__(self, output):
            self.output = output

        def __enter__(self):
            self.output.write('Global\n')
            return self

        def __exit__(self, *e):
            self.output.write('EndGlobal\n')
            pass

    class GlobalSectionContext(contextlib.ContextDecorator):
        class When(Enum):
            Pre = 1
            Post = 2

        __when_mapping = {
            When.Pre:  'preSolution',
            When.Post: 'postSolution'
        }

        def __init__(self, name, when, output):
            self.name = name
            self.when = VSSolution.GlobalSectionContext.__when_mapping[when]
            self.output = output

        def __enter__(self):
            self.output.write('\tGlobalSection({0.name}) = {0.when}\n'.format(self))
            return self

        def __exit__(self, *e):
            self.output.write('\tEndGlobalSection\n')

    def __write_sln(self):
        with open(self.file_path, 'w', encoding='utf-8') as output:
            output.write('\ufeff\nMicrosoft Visual Studio Solution File, Format Version 12.00\n')
            output.write('# Visual Studio 15\n')
            output.write('MinimumVisualStudioVersion = 10.0.40219.1\n')

            for project in self.projects:
                with VSSolution.ProjectContext(project, output):
                    pass

            with VSSolution.GlobalContext(output):
                with VSSolution.GlobalSectionContext('SolutionConfigurationPlatforms', VSSolution.GlobalSectionContext.When.Pre, output):
                    for config_name in Types.config_mapping.values():
                        params = {'solution': self, 'config': config_name}
                        output.write('\t\t{config}|{solution.platform_name} = {config}|{solution.platform_name}\n'.format(**params))

                with VSSolution.GlobalSectionContext('ProjectConfigurationPlatforms', VSSolution.GlobalSectionContext.When.Post, output):
                    for project in self.projects:
                        for config_name in Types.config_mapping.values():
                            params = {'solution': self, 'project': project, 'config': config_name}
                            output.write('\t\t{{{project.guid}}}.{config}|{solution.platform_name}.ActiveCfg = {config}|{project.platform_name}\n'.format(**params))
                            output.write('\t\t{{{project.guid}}}.{config}|{solution.platform_name}.Build.0   = {config}|{project.platform_name}\n'.format(**params))

            with VSSolution.GlobalSectionContext('SolutionProperties', VSSolution.GlobalSectionContext.When.Pre, output):
                output.write('\t\tHideSolutionNode = FALSE\n')

            with VSSolution.GlobalSectionContext('ExtensibilityGlobals', VSSolution.GlobalSectionContext.When.Post, output):
                output.write('\t\tSolutionGuid = {{{}}}\n'.format(self.guid))

    def write(self):
        self.__write_sln()
        for project in self.projects:
            project.write()


def write_solution(solution: Structure.Solution, context: Common.Context):
    assert(context.platform in Types.solution_platform_mapping)
    assert(context.platform in Types.project_platform_mapping)

    vs_context = VSContext(solution, context)
    vs_solution = VSSolution(vs_context)
    vs_solution.write()
