import uuid


class GuidCache:
    def __init__(self, db):
        self.data = {}
        self.db   = db
        self._prepare_db()

    def get_guid(self, key: str):
        if key in self.data:
            return self.data[key]
        else:
            return self._generate_guid(key)

    def set_guid(self, key: str, guid: str):
        self.data[key] = guid
        self._save_guid(key, guid)

    def _generate_guid(self, key: str):
        guid = uuid.uuid1()
        self.set_guid(key, str(guid))
        return guid

    def _prepare_db(self):
        with self.db:
            self.db.execute('CREATE TABLE IF NOT EXISTS "guids" ("key" TEXT PRIMARY KEY, "value" TEXT);')
            for entry in self.db.execute('SELECT * FROM "guids";'):
                self.data[entry[0]] = entry[1]

    def _save_guid(self, key: str, guid: str):
        with self.db:
            self.db.execute('INSERT OR REPLACE INTO "guids" VALUES("{0}", "{1}");'.format(key, guid))
