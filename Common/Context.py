import argparse
from enum import Enum


class Context:
    version = [0, 1]

    class Platform(Enum):
        Win32 = 1
        Win64 = 2

    platform_names = {
        Platform.Win32: 'Win32',
        Platform.Win64: 'Win64'
    }

    platform_mapping = {
        'Win32': Platform.Win32,
        'Win64': Platform.Win64
    }

    class Config(Enum):
        Debug   = 1
        Release = 2

    config_names = {
        Config.Debug:   'Debug',
        Config.Release: 'Release'
    }

    config_mapping = {
        'Debug':   Config.Debug,
        'Release': Config.Release
    }

    def __init__(self, args=None):
        arg_parser = argparse.ArgumentParser(
            description='Project builder application. It can generate projects for various build environments.')
        arg_parser.add_argument(
            '-p', '--platform',
            help='sets the platform for project generation',
            required=True,
            choices=Context.platform_mapping.keys()
        )
        arg_parser.add_argument(
            '-i', '--input',
            help='the input xml to generate from',
            required=True,
        )
        arg_parser.add_argument(
            '-o', '--output',
            help='the target directory for the generated solution',
            required=True
        )
        arg_parser.add_argument(
            '-v', '--version',
            help='prints the version informations',
            action='version',
            version='%(prog)s {}.{}'.format(*Context.version)
        )
        arg_parser.add_argument(
            '-n', '--name',
            help='the name of the solution'
        )
        arg_parser.add_argument(
            '-s', '--start_up',
            help='name of the default startup project'
        )
        arg_parser.add_argument(
            '-l', '--log_path',
            default='ProjectBuilder.log',
            help='path to the log file(default: ProjectBuilder.log)'
        )
        arg_parser.add_argument(
            '-L', '--log_level',
            choices=['CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG'],
            default='WARNING',
            help='log level(default: WARNING)'
        )
        self.args = arg_parser.parse_args(args)

        self.platform = Context.platform_mapping[self.args.platform]
        self.config   = None

    def check_platform(self, platform):
        return platform is self.platform

    def check_platforms(self, platforms):
        for platform in platforms:
            if not self.check_platform(platform):
                return False
        return True

    def check_config(self, config):
        return config is self.config

    def check_configs(self, configs):
        for config in configs:
            if not self.check_config(config):
                return False
        return True

    def check(self, platforms, configs):
        return self.check_platforms(platforms) and self.check_configs(configs)

    def __str__(self):
        return ('[Context] {0}\n'
                '  [Args]\n'
                '    Name     : {1.name}\n'
                '    Platform : {1.platform}\n'
                '    Input    : {1.input}\n'
                '    Output   : {1.output}\n'
                '    LogPath  : {1.log_path}\n'
                '    LogLevel : {1.log_level}\n'
                '  [/Args]\n'
                '  Platform : {2}\n'
                '  Config   : {3}\n'
                '[/Context]').format(
            repr(self),
            self.args,
            self.platform,
            self.config
        )
