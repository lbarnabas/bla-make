from xml.etree import ElementTree as Xml


def indent_xml(element: Xml.Element, level=0):
    indentation = "\n" + level*"  "
    if len(element):
        if not element.text or not element.text.strip():
            element.text = indentation + "  "
        if not element.tail or not element.tail.strip():
            element.tail = indentation
        for element in element:
            indent_xml(element, level + 1)
        if not element.tail or not element.tail.strip():
            element.tail = indentation
    else:
        if level and (not element.tail or not element.tail.strip()):
            element.tail = indentation


def write_xml(element: Xml.Element, path: str):
    indent_xml(element)
    Xml.ElementTree(element).write(path, encoding='utf-8', xml_declaration=True)
