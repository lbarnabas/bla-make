from . import Structure
from .Rules import *

import xml.etree.ElementTree as Xml

import logging
logger = logging.getLogger('ProjectBuilder.Loader')

__all__ = ['load_solution', 'ParseError', 'MissingAttribute', 'UnkownRule']


class ParseError(Exception):
    def __init__(self, message):
        super().__init__(self, message)


class MissingAttribute(ParseError):
    def __init__(self, tag, attribute):
        super().__init__('<{0}> Has no attribute \'{1}\''.format(tag, attribute))


class UnkownRule(ParseError):
    def __init__(self, rule_name):
        super().__init__('Unknown rule: {}'.format(rule_name))


def get_attribute_impl(element: Xml.Element, attribute: str, required: bool):
    if attribute in element.attrib:
        return element.attrib[attribute]
    elif required:
        raise MissingAttribute(element.tag, attribute)


def get_required_attribute(element: Xml.Element, attribute: str):
    return get_attribute_impl(element, attribute, True)


def get_optional_attribute(element: Xml.Element, attribute: str):
    return get_attribute_impl(element, attribute, False)


def get_on_platforms(element: Xml.Element):
    platform_attribute = get_optional_attribute(element, 'on')
    if platform_attribute is None:
        return []
    platforms = []
    for platform_name in platform_attribute.split(';'):
        if platform_name in Common.Context.platform_mapping:
            platforms.append(Common.Context.platform_mapping[platform_name])
        else:
            raise ParseError('<{0}> Unknown platform: {1}'.format(element.tag, platform_name))
    return platforms


def get_in_configs(element: Xml.Element):
    config_attribute = get_optional_attribute(element, 'in')
    if config_attribute is None:
        return []
    configs = []
    for config_name in config_attribute.split(';'):
        if config_name in Common.Context.config_mapping:
            configs.append(Common.Context.config_mapping[config_name])
        else:
            raise ParseError('<{0}> Unknown config: {1}'.format(element.tag, config_name))
    return configs


def parse_set(set_element):
    name = get_required_attribute(set_element, 'name')
    value = get_required_attribute(set_element, 'value')
    platforms = get_on_platforms(set_element)
    configs = get_in_configs(set_element)
    return name, value, platforms, configs


def parse_include_directory(rule_element):
    path = get_required_attribute(rule_element, 'path')
    return IncludeDirectoryRule(path)


def parse_rule(rule_element):
    if rule_element.tag == 'IncludeDirectory':
        return parse_include_directory(rule_element)
    else:
        raise UnkownRule(rule_element.tag)


def parse_rule_set(rule_set_element):
    name = get_optional_attribute(rule_set_element, 'name')
    rule_set = RuleSet()
    for element in rule_set_element:
        platforms = get_on_platforms(element)
        configs = get_in_configs(element)
        rule_set.add_rule(parse_rule(element), platforms, configs)
    return name, rule_set


output_type_mapping = {
    'StaticLibrary':  Structure.Project.OutputType.StaticLibrary,
    'DynamicLibrary': Structure.Project.OutputType.DynamicLibrary,
    'Application':    Structure.Project.OutputType.Application
}


def parse_output_type(output_type_element):
    value = get_required_attribute(output_type_element, 'value')
    if value in output_type_mapping:
        return output_type_mapping[value]
    else:
        raise ParseError('<OutputType> Invalid value: {}'.format(value))


def parse_apply_rule(apply_rule_element):
    return get_required_attribute(apply_rule_element, 'name')


def parse_depends_on(depends_on_element):
    return get_required_attribute(depends_on_element, 'name')


def parse_folder(folder_element):
    new_folder = Structure.Folder()
    new_folder.name = get_required_attribute(folder_element, 'name')
    parse_sub_folders(folder_element, new_folder)
    return new_folder


pch_mapping = {
    'Auto':    Structure.File.Pch.Auto,
    'Header':  Structure.File.Pch.Header,
    'Create':  Structure.File.Pch.Create,
    'Disable': Structure.File.Pch.Disable
}


def parse_pch(file_element):
    pch = get_optional_attribute(file_element, 'pch')
    if pch in pch_mapping:
        return pch_mapping[pch]
    return Structure.File.Pch.Auto


def parse_file(file_element, file_type):
    new_file = Structure.File()

    new_file.file_path = get_required_attribute(file_element, 'path')
    new_file.file_type = file_type
    new_file.pch = parse_pch(file_element)
    new_file.platforms = get_on_platforms(file_element)

    return new_file


def parse_header(file_element):
    return parse_file(file_element, Structure.File.Type.Header)


def parse_source(file_element):
    return parse_file(file_element, Structure.File.Type.Source)


def parse_exclusion(exclusion_element):
    path = get_required_attribute(exclusion_element, 'path')
    platforms = get_on_platforms(exclusion_element)
    return path, platforms


def parse_mirror(mirror_element):
    mirror = Structure.Mirror()

    mirror.name = get_optional_attribute(mirror_element, 'name')
    mirror.path = get_required_attribute(mirror_element, 'path')
    for element in mirror_element:
        if element.tag == 'Exclude':
            mirror.exclusions.append(parse_exclusion(element))

    return mirror


def parse_sub_folders(folder_element, parent_folder):
    for element in folder_element:
        if element.tag == 'Folder':
            parent_folder.folders.append(parse_folder(element))
        elif element.tag == 'Header':
            parent_folder.files.append(parse_header(element))
        elif element.tag == 'Source':
            parent_folder.files.append(parse_source(element))
        elif element.tag == 'Mirror':
            parent_folder.mirrors.append(parse_mirror(element))


def parse_project(project_element):
    project = Structure.Project()

    project.name = get_required_attribute(project_element, 'name')
    project.directory = get_required_attribute(project_element, 'directory')

    for element in project_element:
        if element.tag == 'OutputType':
            project.output_type = parse_output_type(element)
        elif element.tag == 'ApplyRule':
            project.global_rules.append(parse_apply_rule(element))
        elif element.tag == 'DependsOn':
            project.dependencies.append(parse_depends_on(element))
        elif element.tag == 'RuleSet':
            name, rule_set = parse_rule_set(element)
            project.local_rules.append_rule_set(rule_set)
        elif element.tag == 'Files':
            parse_sub_folders(element, project.root_folder)

    return project


def parse_solution(solution: Structure.Solution, root_element: Xml.Element):
    if solution.name is None:
        solution.name = get_required_attribute(root_element, 'name')

    for element in root_element:
        if element.tag == 'Set':
            name, value, platforms, configs = parse_set(element)
            solution.add_global_variable(name, value, platforms, configs)
        elif element.tag == 'RuleSet':
            name, rule_set = parse_rule_set(element)
            if name is None:
                raise MissingAttribute('RuleSet', 'name')
            solution.global_rules[name] = rule_set
        elif element.tag == 'Project':
            solution.projects.append(parse_project(element))


def load_solution(solution: Structure.Solution, xml_path):
    tree = Xml.parse(xml_path)
    parse_solution(solution, tree.getroot())
