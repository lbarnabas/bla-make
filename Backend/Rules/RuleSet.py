from ... import Common


class RuleSet:
    def __init__(self):
        self.__rules = []

    def get_rules(self, rule_type, context: Common.Context):
        rules = []
        for rule, platforms, configs in self.__rules:
            if isinstance(rule, rule_type) and context.check(platforms, configs):
                rules.append(rule)
        return rules

    def add_rule(self, rule, platforms, configs):
        self.__rules.append((rule, platforms, configs))

    def append_rule_set(self, rule_set):
        self.__rules += rule_set.__rules
