from .Rule import Rule


class IncludeDirectoryRule(Rule):
    def __init__(self, directory):
        super().__init__()
        self.directory = directory

    def __str__(self):
        return "<IncludeDirectory> {}".format(self.directory)
