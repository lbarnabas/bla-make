from ... import Common

from ..Structure import *

import os


class Folder:
    def __init__(self):
        self.name = ''
        self.files = []
        self.folders = []
        self.mirrors = []

    def is_empty(self):
        return len(self.files) == 0 and len(self.folders) == 0 and len(self.mirrors) == 0

    def get_files(self, context: Common.Context, project_directory: str):
        converted_folder = Folder()
        converted_folder.name = self.name

        for file in self.files:
            if context.check_platforms(file.platforms):
                converted_file = File()

                converted_file.file_path = os.path.join(project_directory, file.file_path)
                converted_file.file_type = file.file_type
                converted_file.pch = file.pch
                converted_file.platforms = file.platforms

                converted_folder.files.append(converted_file)

        for sub_folder in self.folders:
            converted_sub_folder = sub_folder.get_files(context, project_directory)
            if not converted_sub_folder.is_empty():
                converted_folder.folders.append(converted_sub_folder)

        for mirror in self.mirrors:
            converted_sub_folder = mirror.get_files(context, project_directory)
            if not converted_sub_folder.is_empty():
                if mirror.name is None:
                    converted_folder.append(converted_sub_folder)
                else:
                    converted_sub_folder.name = mirror.name
                    converted_folder.folders.append(converted_sub_folder)

        assert(len(converted_folder.mirrors) == 0)
        return converted_folder

    def append(self, folder):
        self.files += folder.files
        self.folders += folder.folders
        self.mirrors += folder.mirrors
