from enum import Enum


class File:
    class Type(Enum):
        Header = 1,
        Source = 2,

    class Pch(Enum):
        Auto = 1,
        Header = 2,
        Create = 3,
        Disable = 4,

    def __init__(self):
        self.file_path = ''
        self.file_type = File.Type.Source
        self.pch = File.Pch.Auto
        self.platforms = []
