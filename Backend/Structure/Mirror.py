from ... import Common

from ..Structure import *

import os

import logging
logger = logging.getLogger('ProjectBuilder.Mirror')


class Exclusion:
    def __init__(self, path):
        self.path = path


class Mirror:
    def __init__(self):
        self.name = None
        self.path = ""
        self.exclusions = []

    def get_files(self, context: Common.Context, project_directory: str):
        return self.__mirror_on_path(os.path.join(project_directory, self.path), context, Mirror.__MirroringContext(project_directory, self.exclusions))

    extension_mapping = {
        Common.Context.Platform.Win32: {'.h': File.Type.Header, '.hpp': File.Type.Header, '.c': File.Type.Source, '.cpp': File.Type.Source},
        Common.Context.Platform.Win64: {'.h': File.Type.Header, '.hpp': File.Type.Header, '.c': File.Type.Source, '.cpp': File.Type.Source}
    }

    class __MirroringContext:
        def __init__(self, directory, exclusions):
            self.__directory = directory
            self.__exclusions = exclusions

        def filter(self, file, context: Common.Context):
            relative_path = os.path.relpath(file.file_path, self.__directory)
            for exclusion, platforms in self.__exclusions:
                if context.check_platforms(platforms) and relative_path.startswith(exclusion):
                    return False
            return True

    def __mirror_on_path(self, path: str, context: Common.Context, mirroring: __MirroringContext):
        this_folder = Folder()
        this_folder.name = os.path.basename(path)

        extension_mapping = Mirror.extension_mapping[context.platform]

        for subdir, dirs, files in os.walk(path):
            for file in files:
                file_extension = os.path.splitext(file)[1]

                if file_extension in extension_mapping:
                    this_file = File()

                    this_file.file_path = os.path.join(path, file)
                    this_file.file_type = extension_mapping[file_extension]
                    this_file.platforms = []

                    if mirroring.filter(this_file, context):
                        this_folder.files.append(this_file)

            for dir_path in dirs:
                sub_folder = self.__mirror_on_path(os.path.join(path, dir_path), context, mirroring)
                if not sub_folder.is_empty():
                    this_folder.folders.append(sub_folder)

            break

        assert(len(this_folder.mirrors) == 0)
        return this_folder
