from ... import Common

from .. import Rules
from .. import Structure

from enum import Enum


class Project:
    class OutputType(Enum):
        StaticLibrary = 1
        DynamicLibrary = 2
        Application = 3

    def __init__(self):
        self.name = ''
        self.directory = ''
        self.output_type = Project.OutputType.StaticLibrary
        self.local_rules = Rules.RuleSet()
        self.global_rules = []
        self.dependencies = []
        self.root_folder = Structure.Folder()

    def get_includes(self, solution, context: Common.Context):
        local_rules = self.local_rules.get_rules(Rules.IncludeDirectoryRule, context)
        for global_rule_name in self.global_rules:
            local_rules += solution.global_rules[global_rule_name].get_rules(Rules.IncludeDirectoryRule, context)
        return local_rules

    def get_files(self, solution, context: Common.Context):
        return self.root_folder.get_files(context, solution.decode_globals(self.directory, context))

    def __str__(self):
        return ('[Project] {0}\n'
                '  Name        : {1.name}\n'
                '  Directory   : {1.directory}\n'
                '  OutputType  : {1.output_type}\n'
                '  Global rules: {1.global_rules}\n'
                '  Local rules : {1.local_rules}\n'
                '  Dependencies: {1.dependencies}\n'
                '[/Project]'
                ).format(repr(self), self)
