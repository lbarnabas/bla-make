from ... import Common

import re

__all__ = ['Solution', 'UnsetVariable']


class UnsetVariable(Exception):
    def __init__(self, variable_name, context):
        super().__init__('Variable \'{0}\' is not set on platform({1}) in config({2})', variable_name, Common.Context.platform_mapping[context.platform], Common.Context.config_mapping[context.config])


class Solution:
    def __init__(self):
        self.name = None
        self.global_variables = {}
        self.global_rules = {}
        self.projects = []

        self.pattern = re.compile('{[A-Za-z0-9]+}')

    def decode_globals(self, decodable, context):
        match = self.pattern.search(decodable)
        while match is not None:
            key = decodable[match.start() + 1:match.end() - 1]
            if key not in self.global_variables:
                raise Exception('<Solution> Variable undefined: {0}'.format(key))

            value = self.get_global_variable(key, context)
            decodable = decodable.replace('{{{0}}}'.format(key), value)
            match = self.pattern.search(decodable)

        return decodable

    def add_global_variable(self, variable_name, value, platforms=[], configs=[]):
        new_entry = value, platforms, configs
        if variable_name in self.global_variables:
            self.global_variables[variable_name].append(new_entry)
        else:
            self.global_variables[variable_name] = [new_entry]

    def get_global_variable(self, variable_name, context):
        if variable_name in self.global_variables:
            for value, platforms, configs in self.global_variables[variable_name]:
                if context.check(platforms, configs):
                    return value
        raise UnsetVariable(variable_name, context)

    def __str__(self):
        return ('[Solution] {0}\n'
                '  Name             : {1.name}\n'
                '  Global variables : {1.global_variables}\n'
                '  Global rules     : {1.global_rules}\n'
                '[/Solution]').format(repr(self), self)
